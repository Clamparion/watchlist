// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: '',
    authDomain: 'filmeapp-8edff.firebaseapp.com',
    databaseURL: 'https://filmeapp-8edff.firebaseio.com',
    projectId: 'filmeapp-8edff',
    storageBucket: 'filmeapp-8edff.appspot.com',
    messagingSenderId: '823855744897',
    appId: '1:823855744897:web:98eac68ad7cc700f604a80',
    measurementId: 'G-J05TJRH4RG',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
