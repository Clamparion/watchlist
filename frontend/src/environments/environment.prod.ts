export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyCZbTIr4PHYIlZZxp2-vBtVGvQnv92XOPc',
    authDomain: 'filmeapp-8edff.firebaseapp.com',
    databaseURL: 'https://filmeapp-8edff.firebaseio.com',
    projectId: 'filmeapp-8edff',
    storageBucket: 'filmeapp-8edff.appspot.com',
    messagingSenderId: '823855744897',
    appId: '1:823855744897:web:98eac68ad7cc700f604a80',
    measurementId: 'G-J05TJRH4RG',
  },
};
