import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable, of } from 'rxjs';
import { UserModel } from 'src/app/auth/models/user-model';

export const userCollection = 'users';

@Injectable({
  providedIn: 'root',
})
export class FirestoreUserService {
  constructor(private db: AngularFirestore) {
  }

}

