import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { AngularFireAuth } from '@angular/fire/auth';
import {
  AngularFirestore,
  AngularFirestoreDocument,
} from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { switchMap, take } from 'rxjs/operators';
import { isDefined } from '@angular/compiler/src/util';
import { auth } from 'firebase';
import { UserModel } from 'src/app/auth/models/user-model';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  user: Observable<UserModel>;

  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router
  ) {
    this.user = this.afAuth.authState.pipe(
      switchMap((user) => {
        if (isDefined(user)) {
          return this.afs.doc<UserModel>(`users/${user.uid}`).valueChanges();
        } else {
          return of(null);
        }
      })
    );
  }

  public getUserAsPromise(): Promise<UserModel> {
    return new Promise<UserModel>((resolve, reject) => {
      this.user.pipe(take(1)).subscribe(
        (user) => {
          resolve(user);
        },
        (err) => reject(err)
      );
    });
  }

  async signInWithGoogle() {
    const provider = new auth.GoogleAuthProvider();
    const credential = await this.afAuth.auth.signInWithPopup(provider);
    const userData = this.createUserData(credential.user);
    return this.updateUserData(userData);
  }

  async signInWithEmailAndPassword(email: string, password: string) {
    const credential = await this.afAuth.auth.signInWithEmailAndPassword(
      email,
      password
    );
    return this.afs.doc(
      `users/${credential.user.uid}`
    ) as AngularFirestoreDocument<UserModel>;
  }

  async registerWithEmailAndPassword(email: string, password: string, userName: string) {
    const credential = await this.afAuth.auth.createUserWithEmailAndPassword(
      email,
      password
    );
    const userData = this.createUserData(credential.user);
    userData.displayName = userName;
    return this.updateUserData(userData);
  }

  async signOut() {
    await this.afAuth.auth.signOut();
    return this.router.navigate(['/login']);
  }


  private createUserData(user: UserModel): UserModel {
    return {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: isDefined(user.photoURL)
        ? user.photoURL
        : null,
    };
  }

  private updateUserData(user: UserModel) {
    const userRef: AngularFirestoreDocument<UserModel> = this.afs.doc(
      `users/${user.uid}`
    );
    return userRef.set(user, { merge: true });
  }
}
