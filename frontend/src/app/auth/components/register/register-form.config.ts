import { FormlyFieldConfig } from '@ngx-formly/core';
import { emailPattern } from 'src/app/form-validation-messages';

export function generateRegisterForm(
): FormlyFieldConfig[] {
  return [
    {
      fieldGroupClassName: 'row',
      fieldGroup: [
        {
          key: 'name',
          type: 'input',
          className: 'col-12',
          templateOptions: {
            label: 'Benutzername',
            placeholder: 'Benutzername eingeben',
            required: true,
            minLength: 3
          },
        },
        {
          key: 'email',
          type: 'input',
          className: 'col-12',
          templateOptions: {
            label: 'Email',
            placeholder: 'Email-Adresse eingeben',
            required: true,
            pattern: emailPattern
          },
        },
        {
          key: 'password',
          type: 'input',
          className: 'col-12',
          templateOptions: {
            type: 'password',
            label: 'Passwort',
            placeholder: 'Passwort eingeben',
            required: true,
            minLength: 6
          },
        },
      ],
    },
  ];
}
