import { Component, OnInit } from '@angular/core';
import { UserLogin, UserRegister } from 'src/app/auth/models/user-login-model';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AuthService } from 'src/app/auth/services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { isDefined } from '@angular/compiler/src/util';
import { generateRegisterForm } from 'src/app/auth/components/register/register-form.config';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  model: UserRegister = { email: undefined, password: undefined, name: undefined };
  options: FormlyFormOptions = {};
  fields: FormlyFieldConfig[] = [];
  registerForm: FormGroup;
  constructor(public fb: FormBuilder, public auth: AuthService, private toastr: ToastrService, private readonly router: Router) {
    this.registerForm = fb.group({});
  }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.fields = generateRegisterForm();
    if (isDefined(this.options.resetModel)) {
      this.options.resetModel();
    }
  }

  onSubmit() {
    if (this.registerForm.valid) {
      this.auth
        .registerWithEmailAndPassword(this.model.email, this.model.password, this.model.name)
        .then(() => {
          this.toastr.success('Du hast dich erfolgreich registriert, warte bis du freigeschaltet wirst.', 'Erfolgreich registriert');
          this.router.navigate(['/login']);
        })
        .catch((err) => {
          this.toastr.error(err.message, 'Fehler beim Registrieren');
          console.log(err.message);
        });
    }
  }

}
