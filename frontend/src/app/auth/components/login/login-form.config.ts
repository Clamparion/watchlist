import { FormlyFieldConfig } from '@ngx-formly/core';
import { emailPattern } from 'src/app/form-validation-messages';

export function generateLoginForm(
): FormlyFieldConfig[] {
  return [
    {
      fieldGroupClassName: 'row',
      fieldGroup: [
        {
          key: 'email',
          type: 'input',
          className: 'col-12',
          templateOptions: {
            label: 'Email',
            placeholder: 'Email-Adresse eingeben',
            required: true,
            pattern: emailPattern
          },
        },
        {
          key: 'password',
          type: 'input',
          className: 'col-12',
          templateOptions: {
            type: 'password',
            label: 'Passwort',
            placeholder: 'Passwort eingeben',
            required: true,
            minLength: 6
          },
        },
      ],
    },
  ];
}

