import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/services/auth.service';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { UserLogin } from 'src/app/auth/models/user-login-model';
import { generateLoginForm } from 'src/app/auth/components/login/login-form.config';
import { isDefined } from '@angular/compiler/src/util';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  model: UserLogin = { email: undefined, password: undefined };
  options: FormlyFormOptions = {};
  fields: FormlyFieldConfig[] = [];
  loginForm: FormGroup;
  constructor(
    public fb: FormBuilder,
    public auth: AuthService,
    private toastr: ToastrService,
    private router: Router
  ) {
    this.loginForm = fb.group({});
  }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.fields = generateLoginForm();
    if (isDefined(this.options.resetModel)) {
      this.options.resetModel();
    }
  }

  signInWithGoogle() {
    this.auth
      .signInWithGoogle()
      .then(() => this.router.navigate(['']))
      .catch((err) => {
        this.toastr.error(err.message, 'Fehler beim Login');
      });
  }

  onSubmit() {
    if (this.loginForm.valid) {
      this.auth
        .signInWithEmailAndPassword(this.model.email, this.model.password)
        .then(() =>  {
          this.router.navigate(['']);
        }  )
        .catch((err) => {
          this.toastr.error(err.message, 'Fehler beim Login');
        });
    }
  }
}
