import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { AuthService } from 'src/app/auth/services/auth.service';
import { take, map, switchMap, tap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { isDefined } from '@angular/compiler/src/util';

export const burgerUserData = 'burgeruserdata';

@Injectable({
  providedIn: 'root',
})
export class FirestoreUserDataService {
  private userId: string;

  constructor(private db: AngularFirestore, private auth: AuthService) {
    this.loadUserId().subscribe((uid) => {
      this.userId = uid;
    });
  }

  private loadUserId(): Observable<string> {
    if (isDefined(this.userId)) {
      return of(this.userId);
    } else {
      return this.auth.user.pipe(
        take(1),
        map((u) => u.uid),
        tap((uid) => (this.userId = uid))
      );
    }
  }

  public remove(collection: string, id: string): Promise<void> {
    if (isDefined(this.userId) && isDefined(id)) {
      return this.db
        .doc(`${burgerUserData}/${this.userId}/${collection}/${id}`)
        .delete();
    }
  }

  public listAll<T>(collection: string): Observable<T[]> {
    return this.loadUserId().pipe(
      switchMap((uid) => {
        if (isDefined(uid)) {
          return this.db
            .collection<T>(`${burgerUserData}/${uid}/${collection}`)
            .valueChanges({ idField: 'mediaId' });
        } else {
          return of([]);
        }
      })
    );
  }

  public add(collection: string, data: any): Promise<DocumentReference> {
    return this.db
      .collection(`${burgerUserData}/${this.userId}/${collection}`)
      .add(data);
  }
}
