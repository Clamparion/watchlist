import { Injectable } from '@angular/core';
import { AngularFireFunctions } from '@angular/fire/functions';
import { Observable, of } from 'rxjs';
import {
  ImdbApiSearchResultReadable,
} from 'src/app/imdb/imdb-model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ImdbService {
  constructor(private fns: AngularFireFunctions) {}

  search(text: string): Observable<ImdbApiSearchResultReadable[]> {
    if (text === undefined) {
      return of([]);
    }
    const callable = this.fns.httpsCallable<any, ImdbApiSearchResultReadable[]>(
      'imdb'
    );
    return callable({ s: text.trim() });
  }
}
