import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { OmdbResult } from 'src/app/imdb/imdb-model';

const API_URL = 'https://www.omdbapi.com?apikey=77d37e9a';

@Injectable({
  providedIn: 'root'
})
export class OmdbService {
  constructor(private readonly http: HttpClient) { }

  detailsWithImdbId(imdbId: string): Observable<OmdbResult> {
    return this.http.get<OmdbResult>(`${API_URL}&i=${imdbId}`);
  }
}


