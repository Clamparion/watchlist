import { Component, Input, OnChanges, SimpleChanges, TemplateRef } from '@angular/core';
import { Observable } from 'rxjs';

import { FirestoreWatchlistService } from '../services/firestore-watchlist.service';
import { Watchlist, WatchlistDetails } from './../watchlist-overview-model';

@Component({
  selector: 'app-watchlist-card',
  templateUrl: './watchlist-card.component.html',
  styleUrls: ['./watchlist-card.component.scss']
})
export class WatchlistCardComponent implements OnChanges {

  @Input() watchlist: Watchlist;

  constructor(private readonly watchlistsService: FirestoreWatchlistService) { }
  details: Observable<WatchlistDetails>;
  shareModalOpened = false;

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.watchlist) {
     this.details = this.watchlistsService.getWatchlistDetails(this.watchlist.watchlistId);
    }
  }




}
