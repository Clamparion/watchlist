import { isDefined } from '@angular/compiler/src/util';
import { AddWatchlist } from './../watchlist-overview-model';
import {
  Watchlist,
  UserWithWatchlistsIds,
  UserWithWatchlists,
  WatchlistDetails,
} from '../watchlist-overview-model';
import { switchMap, map } from 'rxjs/operators';
import { AuthService } from '../../auth/services/auth.service';

import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { Observable, of } from 'rxjs';
import * as firebase from 'firebase';
import { Media, MediaType } from 'src/app/watchlist/media-model';

@Injectable({
  providedIn: 'root',
})
export class FirestoreWatchlistService {
  constructor(
    private db: AngularFirestore,
    private readonly authService: AuthService
  ) {}

  private getUserWithWatchlistsIds(): Observable<UserWithWatchlistsIds> {
    return this.authService.user.pipe(
      switchMap((user) =>
        this.db
          .collection('users')
          .doc<UserWithWatchlistsIds>(user.uid)
          .valueChanges()
      )
    );
  }

  private getWatchlistsWithIds(
    watchlistIds: string[]
  ): Observable<Watchlist[]> {
    if (isDefined(watchlistIds) && watchlistIds.length > 0) {
      return this.db
      .collection<Watchlist>('watchlists', (ref) =>
        ref.where(firebase.firestore.FieldPath.documentId(), 'in', watchlistIds)
      )
      .valueChanges({ idField: 'watchlistId' });
    } else {
      return of([]);
    }

  }

  public getWatchlist(
    watchlistId: string
  ): Observable<Watchlist> {
    return this.db
      .collection(`watchlists`).doc<Watchlist>(watchlistId).valueChanges();
  }

  public getWatchlistDetails(
    watchlistId: string
  ): Observable<WatchlistDetails> {
    return this.db
      .collection<Media>(`watchlists/${watchlistId}/media`)
      .valueChanges()
      .pipe(
        map((mediaList) => {
          return {
            numberOfMovies: mediaList.filter((m) => m.type === MediaType.Movie)
              .length,
            numberOfSeries: mediaList.filter((m) => m.type === MediaType.Series)
              .length,
            firstFourMedia: mediaList.slice(0, 4),
          } as WatchlistDetails;
        })
      );
  }

  public getUserWithWatchlists(): Observable<UserWithWatchlists> {
    return this.getUserWithWatchlistsIds().pipe(
      switchMap((userWithIds) =>
        this.getWatchlistsWithIds(userWithIds.watchlists).pipe(
          map(
            (wachtlists) =>
              new UserWithWatchlists(userWithIds.displayName, wachtlists)
          )
        )
      )
    );
  }

  public assignWatchListToUser(watchlistId: string, userId: string): Promise<void> {
    return this.db.collection('users').doc(userId).update({
      watchlists: firebase.firestore.FieldValue.arrayUnion(watchlistId)
    });
  }

  public assignUserToWatchList(watchlistId: string, userId: string): Promise<void> {
    return this.db.collection('watchlists').doc(watchlistId).update({
      users: firebase.firestore.FieldValue.arrayUnion(userId)
    });
  }

  public addWatchlist(watchlist: AddWatchlist): Promise<DocumentReference> {
    return this.db.collection('watchlists').add({
      name: watchlist.name,
      users: watchlist.users,
    });
  }
}
