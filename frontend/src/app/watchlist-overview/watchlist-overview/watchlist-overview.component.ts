import { ToastrService } from 'ngx-toastr';
import { takeUntil } from 'rxjs/operators';
import { DestroySubs } from 'src/app/destroy.sub';
import { Component, OnInit } from '@angular/core';
import { FirestoreWatchlistService } from '../services/firestore-watchlist.service';
import { UserWithWatchlists } from '../watchlist-overview-model';
import { Observable } from 'rxjs';
import { WatchlistInviteService } from 'src/app/watchlist-invite/watchlist-invite.service';

@Component({
  selector: 'app-watchlist-overview',
  templateUrl: './watchlist-overview.component.html',
  styleUrls: ['./watchlist-overview.component.scss'],
})
export class WatchlistOverviewComponent implements OnInit {
  userWithWatchlist: Observable<UserWithWatchlists>;

  constructor(
    private readonly watchlists: FirestoreWatchlistService,
    watchlistInviteService: WatchlistInviteService,
    private readonly toastr: ToastrService
  ) {
    watchlistInviteService.processInvitation().then((newWatchlistAdded) => {
      if (newWatchlistAdded) {
        toastr.success(
          'Watchlist hinzugefügt!',
          'Es wurde eine neue Watchlist zu deiner Auswahl hinzugefügt.'
        );
      }
      this.userWithWatchlist = this.watchlists.getUserWithWatchlists();
    });
  }

  ngOnInit(): void {}
}
