
import { Media } from '../watchlist/media-model';

export class UserWithWatchlistsIds {
  displayName: string;
  watchlists: string[];
}

export class Watchlist {
  watchlistId: string;
  name: string;
  users: string[];
}


export class AddWatchlist {
  name: string;
  users: string[];
}


export class WatchlistDetails {
  numberOfMovies: number;
  numberOfSeries: number;
  firstFourMedia: Media[];
}

export class UserWithWatchlists {
  constructor(public displayName: string, public watchlists: Watchlist[]) {
  }
}
