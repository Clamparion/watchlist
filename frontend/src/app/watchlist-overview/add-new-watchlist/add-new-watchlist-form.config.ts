
import { FormlyFieldConfig } from '@ngx-formly/core';
import { validateName } from 'src/app/watchlist/add-media-wizard/add-media-form/add-media-form.config';

export function generateAddNewWatchlistForm(
  otherWatchlistNames: string[]
): FormlyFieldConfig[] {
  return [
    {
      fieldGroupClassName: 'row',
      fieldGroup: [
        {
          key: 'name',
          type: 'input',
          className: 'col-xl-6 col-12',
          templateOptions: {
            label: 'Name',
            placeholder: 'Name der Watchlist',
            required: true,
            minLength: 2,
          },
          validators: {
            ipv: {
              expression: validateName(otherWatchlistNames),
              message: (error, field: FormlyFieldConfig) =>
                `Eine Watchlist mit dem Namen "${field.formControl.value}" existiert bereits`,
            },
          },
        }
      ],
    },
  ];
}

