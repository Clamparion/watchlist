import { DocumentReference } from '@angular/fire/firestore';
import { ToastrService } from 'ngx-toastr';
import { UserModel } from './../../auth/models/user-model';
import { AuthService } from './../../auth/services/auth.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Component } from '@angular/core';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';

import { take } from 'rxjs/operators';
import { AddWatchlist } from '../watchlist-overview-model';
import { FirestoreWatchlistService } from '../services/firestore-watchlist.service';
import { generateAddNewWatchlistForm } from './add-new-watchlist-form.config';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-new-watchlist',
  templateUrl: './add-new-watchlist.component.html',
  styleUrls: ['./add-new-watchlist.component.scss'],
})
export class AddNewWatchlistComponent {
  watchlistForm: FormGroup;
  model: AddWatchlist;
  options: FormlyFormOptions = {};
  fields: FormlyFieldConfig[] = [];
  isLoading = false;


  constructor(
    private readonly auth: AuthService,
    private readonly watchlists: FirestoreWatchlistService,
    private readonly fb: FormBuilder,
    private readonly toastr: ToastrService,
    private readonly router: Router
  ) {
    this.watchlistForm = fb.group({});
    watchlists
      .getUserWithWatchlists()
      .pipe(take(1))
      .subscribe((res) => {
        this.fields = generateAddNewWatchlistForm(res.watchlists.map(wl => wl.name));
      });
    this.auth.user.pipe(take(1)).subscribe((usr) => {
      this.initModel(usr);
    });
  }

  private initModel(user: UserModel) {
    this.model = new AddWatchlist();
    this.model.name = null;
    this.model.users = [user.uid];
  }


  private async generateNewWatchList(): Promise<DocumentReference> {
    const newWl = await this.watchlists.addWatchlist(this.model);
    await this.watchlists.assignWatchListToUser(newWl.id, this.model.users[0]);
    return newWl;
  }

  public onSubmit() {
    if (this.watchlistForm.valid) {
      this.isLoading = true;
      this.generateNewWatchList().then(res => {
        this.router.navigate(['']);
      }).catch(err => {
        this.toastr.error(err, 'Watchlist konnte nicht hinzugefügt werden');
      }).finally(() => this.isLoading = false);
    }

  }
}
