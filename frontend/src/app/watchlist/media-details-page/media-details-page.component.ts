import { MediaService } from './../services/media.service';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { FirestoreMediaService } from 'src/app/watchlist/services/firestore-media.service';
import { Component, OnInit, Input } from '@angular/core';
import { Media } from '../media-model';
import { FirestoreWatchedMediaService } from '../services/firestore-watched-media.service';

@Component({
  selector: 'app-media-details-page',
  templateUrl: './media-details-page.component.html',
  styleUrls: ['./media-details-page.component.scss'],
})
export class MediaDetailsPageComponent implements OnInit {
  mediaObs: Observable<Media>;
  mediaId: string;
  watchlistId: string;
  isLoading = false;

  constructor(
    private readonly  mediaService: MediaService,
    private readonly mediaWatched: FirestoreWatchedMediaService,
    private readonly route: ActivatedRoute
  ) {
    route.paramMap.subscribe((res) => {
      this.watchlistId = res.get('id');
      this.mediaId = res.get('mediaId');
      this.mediaObs = this.mediaService.get(this.watchlistId, this.mediaId);
    });
  }

  markAsWatched(item: Media) {
    this.isLoading = true;
    this.mediaWatched
      .markAsWatched(item)
      .then(() => console.log('marked as watched'))
      .catch((err) => console.log(err))
      .finally(() => (this.isLoading = false));
  }

  markAsUnWatched(item: Media) {
    this.mediaWatched
      .markAsUnWatched(item.ownWatchId)
      .then(() => console.log('marked as unwactched'))
      .catch((err) => console.log(err))
      .finally(() => (this.isLoading = false));
  }


  ngOnInit(): void {}
}
