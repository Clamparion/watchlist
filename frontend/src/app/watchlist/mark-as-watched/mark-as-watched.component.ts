import { Component, OnInit, Input } from '@angular/core';
import { FirestoreWatchedMediaService } from '../services/firestore-watched-media.service';
import { Media } from '../media-model';

@Component({
  selector: 'app-mark-as-watched',
  templateUrl: './mark-as-watched.component.html',
  styleUrls: ['./mark-as-watched.component.scss']
})
export class MarkAsWatchedComponent implements OnInit {
  isLoading = false;
  @Input() media: Media;
  constructor(private readonly mediaWatched: FirestoreWatchedMediaService,) { }

  ngOnInit(): void {
  }

  markAsWatched() {
    this.isLoading = true;
    this.mediaWatched
      .markAsWatched(this.media)
      .then(() => console.log('marked as watched'))
      .catch((err) => console.log(err))
      .finally(() => (this.isLoading = false));
  }

  markAsUnWatched() {
    this.mediaWatched
      .markAsUnWatched(this.media.ownWatchId)
      .then(() => console.log('marked as unwactched'))
      .catch((err) => console.log(err))
      .finally(() => (this.isLoading = false));
  }


}
