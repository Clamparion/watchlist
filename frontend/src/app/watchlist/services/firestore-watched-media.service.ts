import { UserModel } from 'src/app/auth/models/user-model';
import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { Observable, of, pipe, from } from 'rxjs';
import { isDefined } from '@angular/compiler/src/util';
import { Media, MediaWatched } from 'src/app/watchlist/media-model';
import { AuthService } from 'src/app/auth/services/auth.service';

export const mediaWatchesCollection = 'mediaWatched';

@Injectable({
  providedIn: 'root',
})
export class FirestoreWatchedMediaService {
  constructor(private db: AngularFirestore, private auth: AuthService) {}

  public markAsUnWatched(id: string): Promise<void> {
    if (isDefined(id)) {
      return this.db.doc(`${mediaWatchesCollection}/${id}`).delete();
    }
  }

  public listAll(): Observable<MediaWatched[]> {
    return this.db.collection<MediaWatched>(`${mediaWatchesCollection}`)
      .valueChanges({ idField: 'watchId' });
  }



  public async markAsWatched(data: Media): Promise<DocumentReference> {
    if (data.imdbId !== undefined) {
      const currentUser = await this.auth.getUserAsPromise();
      await this.db.collection(`${mediaWatchesCollection}/`).doc(data.imdbId + '_' + currentUser.uid).set({
        imdbId: data.imdbId,
        uid: currentUser.uid,
        userName: currentUser.displayName
      });
    }
    return null;
  }
}
