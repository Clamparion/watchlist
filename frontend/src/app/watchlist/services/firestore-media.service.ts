
import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { Observable, of } from 'rxjs';
import { isDefined } from '@angular/compiler/src/util';
import { Media } from 'src/app/watchlist/media-model';


@Injectable({
  providedIn: 'root',
})
export class FirestoreMediaService {
  constructor(private db: AngularFirestore) {
  }

  private getCollectionPath(watchListId) {
    return `watchlists/${watchListId}/media`;
  }

  public remove(watchListId: string, id: string): Promise<void> {
    if (isDefined(id)) {
      return this.db.doc(`${this.getCollectionPath(watchListId)}/${id}`).delete();
    }
  }

  public listAll(watchListId: string): Observable<Media[]> {
    return this.db
      .collection<Media>(`${this.getCollectionPath(watchListId)}`)
      .valueChanges({ idField: 'mediaId' });
  }

  public get(watchListId: string, mediaId: string): Observable<Media> {
    return this.db.collection(`${this.getCollectionPath(watchListId)}`).doc<Media>(mediaId).valueChanges();
  }


  public add(watchListId: string, data: Media): Promise<DocumentReference> {
    return this.db.collection(`${this.getCollectionPath(watchListId)}/`).add(data);
  }

  public update(watchListId: string, data: Media): Promise<void> {
    if (isDefined(data.mediaId)) {
      return this.db.doc(`${this.getCollectionPath(watchListId)}/${data.mediaId}`).update(data);
    }
  }

}

