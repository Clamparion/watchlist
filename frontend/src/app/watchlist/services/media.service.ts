import { isDefined } from '@angular/compiler/src/util';
import { Media, MediaWatched, WatchlistAndMediaWrapper } from '../media-model';
import { UserModel } from 'src/app/auth/models/user-model';
import { combineLatest, EMPTY, Observable } from 'rxjs';
import { takeUntil, catchError, map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { FirestoreMediaService } from './firestore-media.service';
import { FirestoreWatchedMediaService } from './firestore-watched-media.service';
import { FirestoreUserService } from 'src/app/auth/services/firestore-user.sevice';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/auth/services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { FirestoreWatchlistService } from 'src/app/watchlist-overview/services/firestore-watchlist.service';
import { DestroySubs } from 'src/app/destroy.sub';
import { rejects } from 'assert';

@Injectable({
  providedIn: 'root',
})
export class MediaService extends DestroySubs {
  constructor(
    private readonly media: FirestoreMediaService,
    private readonly mediaWatched: FirestoreWatchedMediaService,
    private readonly auth: AuthService,
    private readonly toastr: ToastrService,
    private readonly route: ActivatedRoute,
    private readonly watchlists: FirestoreWatchlistService
  ) {
    super();
  }

  assignWatchedUserIdToMedia(
    mediaList: Media[],
    mediaWatchList: MediaWatched[],
    user: UserModel
  ): void {
    const userMediaWatchList = mediaWatchList.filter((m) => m.uid === user.uid);
    mediaList.forEach((m) => {
      m.ownWatchId = undefined;
    });
    mediaList.forEach((m) => {
      userMediaWatchList.forEach((umw) => {
        if (umw.imdbId === m.imdbId) {
          m.ownWatchId = umw.watchId;
        }
      });
    });
  }

  assignWatchedByToMedia(
    mediaList: Media[],
    mediaWatchList: MediaWatched[],
    usersIdsInWatchList: string[]
  ): void {
    const userMediaWatchList = mediaWatchList.filter(
      (m) => usersIdsInWatchList.indexOf(m.uid) !== -1
    );
    mediaList.forEach((m) => {
      m.watchedBy = userMediaWatchList
        .filter((mw) => mw.imdbId === m.imdbId)
        .map((mw) => {
          return mw.userName;
        })
        .filter((a) => isDefined(a));
    });
  }

  sortAlphabeticaly(mediaList: Media[]) {
    mediaList.sort((a: Media, b: Media) => {
      if (a.title < b.title) {
        return -1;
      } else if (a.title > b.title) {
        return 1;
      }
      return 0;
    });
  }

  public get(watchListId: string, mediaId: string): Observable<Media> {
    return combineLatest([
      this.mediaWatched.listAll(),
      this.media.get(watchListId, mediaId),
      this.auth.user,
      this.watchlists.getWatchlist(watchListId),
    ]).pipe(
      takeUntil(this.destroy$),
      catchError((err) => {
        this.toastr.error(err.message, 'Fehler beim Abrufen von Daten!');
        return EMPTY;
      }),
      map(([mediaWatchList, media, user, watchlist]) => {
        const mediaList = [media];
        this.assignWatchedUserIdToMedia(mediaList, mediaWatchList, user);
        this.assignWatchedByToMedia(mediaList, mediaWatchList, watchlist.users);
        this.sortAlphabeticaly(mediaList);
        return media;
      })
    );
  }

  listAll(watchListId: string): Observable<WatchlistAndMediaWrapper> {
    return combineLatest([
      this.mediaWatched.listAll(),
      this.media.listAll(watchListId),
      this.auth.user,
      this.watchlists.getWatchlist(watchListId),
    ]).pipe(
      takeUntil(this.destroy$),
      catchError((err) => {
        this.toastr.error(err.message, 'Fehler beim Abrufen von Daten!');
        return EMPTY;
      }),
      map(([mediaWatchList, mediaList, user, watchlist]) => {
        this.assignWatchedUserIdToMedia(mediaList, mediaWatchList, user);
        this.assignWatchedByToMedia(mediaList, mediaWatchList, watchlist.users);
        this.sortAlphabeticaly(mediaList);

        const wrapperObject = new WatchlistAndMediaWrapper();
        wrapperObject.watchtlist = watchlist;
        wrapperObject.watchtlist.watchlistId = watchListId;
        wrapperObject.media = mediaList;
        return wrapperObject;
      })
    );
  }
}
