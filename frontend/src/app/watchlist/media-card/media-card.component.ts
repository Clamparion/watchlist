import { Router } from '@angular/router';
import { Component, OnInit, Input, TemplateRef } from '@angular/core';
import { Media } from 'src/app/watchlist/media-model';
import { FirestoreMediaService } from 'src/app/watchlist/services/firestore-media.service';
import { FirestoreWatchedMediaService } from 'src/app/watchlist/services/firestore-watched-media.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-movies-card',
  templateUrl: './media-card.component.html',
  styleUrls: ['./media-card.component.scss'],
})
export class MediaCardComponent {
  @Input() media: Media;
  @Input() watchlistId: string;
  modalRef: BsModalRef;
  isLoading = false;

  constructor(
    private readonly mediaSerice: FirestoreMediaService,
    private modalService: BsModalService,
    private router: Router
  ) {}
  deleteItem(item: Media) {
    this.mediaSerice
      .remove(this.watchlistId, item.mediaId)
      .then(() => {})
      .finally(() => this.modalRef.hide());
  }


  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  showDetails() {
    this.router.navigate([`/watchlist/${this.watchlistId}/details/${this.media.mediaId}`]);
  }
}
