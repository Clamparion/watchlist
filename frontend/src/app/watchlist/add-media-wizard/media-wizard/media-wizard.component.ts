import { DestroySubs } from 'src/app/destroy.sub';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FirestoreMediaService } from 'src/app/watchlist/services/firestore-media.service';
import { Observable } from 'rxjs';
import { Media } from 'src/app/watchlist/media-model';
import { tap, map, take, takeUntil } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { isDefined } from '@angular/compiler/src/util';

@Component({
  selector: 'app-movie-wizard',
  templateUrl: './media-wizard.component.html',
  styleUrls: ['./media-wizard.component.scss'],
})
export class MediaWizardComponent extends DestroySubs implements OnInit {
  mediaId: string;
  movieImage: string;
  allNames: Observable<string[]>;
  watchlistId: string;

  constructor(
    private readonly movies: FirestoreMediaService,
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute
  ) {
    super();
  }

  private loadAllNames() {
    this.allNames = this.movies.listAll(this.watchlistId).pipe(
      take(1),
      map((movs) => movs.filter((m) => isDefined(m.title)).map((m) => m.title))
    );
  }

  ngOnInit(): void {
    this.activatedRoute.queryParamMap
      .pipe(takeUntil(this.destroy$))
      .subscribe((params) => {
        this.mediaId = params.get('id');
        this.movieImage = params.get('image');
      });

    // route parameter
    this.activatedRoute.paramMap
      .pipe(takeUntil(this.destroy$))
      .subscribe((params) => {
        if (params.get('id') === undefined) {
          this.router.navigate(['']);
        } else {
          this.watchlistId = params.get('id');
          this.loadAllNames();
        }
      });
  }
}
