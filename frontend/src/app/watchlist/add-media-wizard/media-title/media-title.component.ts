import { Component, OnInit, OnChanges, Input, SimpleChanges } from '@angular/core';
import { AddMedia, MediaType, MediaSearch } from 'src/app/watchlist/media-model';
import { FormGroup, FormBuilder } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { isDefined } from '@angular/compiler/src/util';

import { Observable } from 'rxjs';
import { ImdbApiSearchResultReadable } from 'src/app/imdb/imdb-model';
import { ImdbService } from 'src/app/imdb/services/imdb.service';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';
import { validateName } from 'src/app/watchlist/add-media-wizard/add-media-form/add-media-form.config';

@Component({
  selector: 'app-movie-title',
  templateUrl: './media-title.component.html',
  styleUrls: ['./media-title.component.scss']
})
export class MediaTitleComponent implements OnInit, OnChanges {
  @Input() allNames: string[] = [];
  @Input() watchListId: string;
  movieForm: FormGroup;
  model: MediaSearch;
  isLoading = false;
  options: FormlyFormOptions = {};
  fields: FormlyFieldConfig[] = [];
  searchResults = new Observable<ImdbApiSearchResultReadable[]>();

  constructor(fb: FormBuilder, private imdbService: ImdbService, private readonly router: Router) {
    this.movieForm = fb.group({});
    this.initForm();
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.allNames) {
      this.initForm();
    }
  }

  onSubmit() {
    if (this.movieForm.valid) {
      this.isLoading = true;
      this.searchResults = this.imdbService.search(this.model.name).pipe(finalize(() => this.isLoading = false));
    }
  }

  private resetModel() {
    this.model = {
      name: null,
    } as MediaSearch;
  }

  selectMovie(item: ImdbApiSearchResultReadable) {
    this.router.navigate([`/watchlist/${this.watchListId}/add/`], { queryParams: { id: item.id, image: item.image } });
  }

  initForm() {
    this.resetModel();
    this.fields = [
      {
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            key: 'name',
            type: 'input',
            className: 'col-md-6 col-12',
            templateOptions: {
              label: 'Suchbegriff',
              placeholder: 'Name des Films oder der Serie',
              required: true,
              minLength: 2,
            },
            validators: {
              ipv: {
                expression: validateName(this.allNames),
                message: (error, field: FormlyFieldConfig) =>
                  `Der Film "${field.formControl.value}" existiert bereits`,
              },
            },
          }
        ],
      },
    ];
    if (isDefined(this.options.resetModel)) {
      this.options.resetModel();
    }
  }
}
