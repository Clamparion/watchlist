import { AbstractControl } from '@angular/forms';
import { isDefined } from '@angular/compiler/src/util';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { MediaType } from 'src/app/watchlist/media-model';
import { positiveFloatPattern, positiveIntPattern } from 'src/app/form-validation-messages';

export function generateAddNewMoviesForm(
  otherMovieNames: string[]
): FormlyFieldConfig[] {
  return [
    {
      fieldGroupClassName: 'row',
      fieldGroup: [
        {
          key: 'title',
          type: 'input',
          className: 'col-xl-6 col-12',
          templateOptions: {
            label: 'Titel',
            placeholder: 'Titel des Films',
            required: true,
            minLength: 2,
          },
          validators: {
            ipv: {
              expression: validateName(otherMovieNames),
              message: (error, field: FormlyFieldConfig) =>
                `Der Film "${field.formControl.value}" existiert bereits`,
            },
          },
        },
        {
          key: 'type',
          type: 'select',
          className: 'col-xl-4 col-sm-6 col-12',
          templateOptions: {
            label: 'Typ',
            required: true,
            options: [
              { label: 'Film', value: MediaType.Movie },
              { label: 'Serie', value: MediaType.Series },
            ],
          },
        },
        {
          key: 'genre',
          type: 'input',
          className: 'col-xl-6 col-12',
          templateOptions: {
            label: 'Genre',
            placeholder: 'Genre',
            required: false,
          },
          validators: {},
        },
        {
          key: 'year',
          type: 'input',
          className: 'col-xl-6 col-12',
          templateOptions: {
            label: 'Jahr',
            placeholder: 'Jahr',
            required: false,
            pattern: positiveIntPattern
          },
          validators: {},
        },
        {
          key: 'runtime',
          type: 'input',
          className: 'col-xl-6 col-12',
          templateOptions: {
            label: 'Spielzeit',
            placeholder: 'Spielzeit',
            required: false,
          },
          validators: {},
        },
        {
          key: 'director',
          type: 'input',
          className: 'col-xl-6 col-12',
          templateOptions: {
            label: 'Regie',
            placeholder: 'Regisseur/Regisseurin',
            required: false,
          },
          validators: {},
        },
        {
          key: 'actors',
          type: 'input',
          className: 'col-xl-6 col-12',
          templateOptions: {
            label: 'Schauspieler',
            placeholder: 'Schauspieler',
            required: false,
          },
          validators: {},
        },
        {
          key: 'country',
          type: 'input',
          className: 'col-xl-6 col-12',
          templateOptions: {
            label: 'Land',
            placeholder: 'Land',
            required: false,
          },
          validators: {},
        },
        {
          key: 'awards',
          type: 'input',
          className: 'col-xl-6 col-12',
          templateOptions: {
            label: 'Auszeichnungen',
            placeholder: 'Auszeichnungen',
            required: false,
          },
          validators: {},
        },
        {
          key: 'metascore',
          type: 'input',
          className: 'col-xl-6 col-12',
          templateOptions: {
            label: 'Metascore Bewertung',
            placeholder: 'Metascore',
            required: false,
            pattern: positiveFloatPattern,
          },
          validators: {},
        },
        {
          key: 'imdbRating',
          type: 'input',
          className: 'col-xl-6 col-12',
          templateOptions: {
            label: 'IMDB Bewertung',
            placeholder: 'IxlB Bewertung',
            required: false,
            pattern: positiveFloatPattern,
          },
          validators: {},
        },
        {
          key: 'plot',
          type: 'textarea',
          className: 'col-12',
          templateOptions: {
            label: 'Handlung',
            autosize: true,
            rows: 5,
          },
          validators: {},
        },
      ],
    },
  ];
}

export function validateName(existingNames: string[]) {
  if (!isDefined(existingNames)) {
    return (control: AbstractControl) => {
      return { validName: true };
    };
  }
  const names = existingNames.map((name) => name.toLowerCase().trim());
  return (control: AbstractControl) => {
    if (isDefined(control.value)) {
      const value = control.value.toLowerCase().trim();
      const foundNames = names.filter((n) => n === value);
      if (foundNames.length > 0) {
        return null;
      }
    }
    return { validName: true };
  };
}
