import { Component, Input, OnChanges, SimpleChanges, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Media, AddMedia } from 'src/app/watchlist/media-model';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { isDefined } from '@angular/compiler/src/util';

import {
  isUndefinedValue,
  isEmptyArray,
  convertToFloat,
} from 'src/app/declaredFunctions';

import { AngularFireStorage } from '@angular/fire/storage';
import { finalize } from 'rxjs/operators';
import { Observable } from 'rxjs';


import { OmdbService } from 'src/app/imdb/services/omdb.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/auth/services/auth.service';
import { FirestoreMediaService } from '../../services/firestore-media.service';
import { generateAddNewMoviesForm } from 'src/app/watchlist/add-media-wizard/add-media-form/add-media-form.config';

@Component({
  selector: 'app-add-media-form',
  templateUrl: './add-media-form.component.html',
  styleUrls: ['./add-media-form.component.scss'],
})
export class AddMediaFormComponent implements OnChanges {
  @Input() allNames: string[] = [];
  @Input() mediaId: string;
  @Input() movieImage: string;
  @Input() watchlistId: string;

  userId: string;
  movieForm: FormGroup;
  model: AddMedia;
  options: FormlyFormOptions = {};
  fields: FormlyFieldConfig[] = [];
  isLoading = false;
  omdbLoading = false;


  constructor(
    fb: FormBuilder,
    private readonly media: FirestoreMediaService,
    private readonly angularFireStorage: AngularFireStorage,
    private readonly omdbService: OmdbService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly auth: AuthService
  ) {
    this.movieForm = fb.group({});
    this.initForm();
  }


  ngOnChanges(changes: SimpleChanges): void {
    if (changes.allNames || changes.mediaId || changes.movieImage) {
      this.initForm();
    }
  }

  getImageUrl(): Observable<string> {
    return new Observable<string>((observer) => {
      const n = `${Date.now()}`;
      if (
        isUndefinedValue(this.model.image) ||
        isEmptyArray(this.model.image)
      ) {
        observer.error('selected file is empty');
        return;
      }
      const file = this.model.image[0];
      const filePath = `movieimages/${n}`;
      const fileRef = this.angularFireStorage.ref(filePath);
      const task = this.angularFireStorage.upload(`movieimages/${n}`, file);
      task
        .snapshotChanges()
        .pipe(
          finalize(() => {
            fileRef.getDownloadURL().subscribe(
              (downloadURL) => {
                observer.next(downloadURL);
                observer.complete();
              },
              (err) => observer.error(err)
            );
          })
        )
        .subscribe(
          () => {},
          (err) => observer.error(err)
        );
    });
  }

  private addNewItem() {
    this.isLoading = true;
    const newMovie = ({
      title: this.definedOrNull(this.model.title),
      year: this.definedOrNull(
        isDefined(this.model.year)
          ? parseInt(this.model.year.toString(), 10)
          : null
      ),
      runtime: this.definedOrNull(this.model.runtime),
      genre: this.definedOrNull(this.model.genre),
      director: this.definedOrNull(this.model.director),
      actors: this.definedOrNull(this.model.actors),
      plot: this.definedOrNull(this.model.plot),
      country: this.definedOrNull(this.model.country),
      awards: this.definedOrNull(this.model.awards),
      imdbId: this.definedOrNull(this.model.imdbId),
      metascore: this.definedOrNull(
        isDefined(this.model.metascore)
          ? convertToFloat(this.model.metascore.toString())
          : null
      ),
      imdbRating: this.definedOrNull(
        isDefined(this.model.imdbRating)
          ? convertToFloat(this.model.imdbRating.toString())
          : null
      ),
      type: this.definedOrNull(this.model.type),
      imageUrl: this.definedOrNull(this.model.imageUrl),

    } as unknown) as Media;

    this.auth.user.subscribe((user) => {
      newMovie.addedBy = user.uid;
      console.log(newMovie);
      this.media.add(this.watchlistId, newMovie).then((res) => {
        this.router.navigate([`/watchlist/${this.watchlistId}`]);
      });
    });
  }

  definedOrNull(value: any): any | null {
    return isDefined(value) ? value : null;
  }

  onSubmit() {
    if (this.movieForm.valid) {
      this.addNewItem();
    }
  }

  private resetModel() {
    this.model = new AddMedia(null);
  }

  initForm() {
    this.omdbLoading = true;
    this.omdbService.detailsWithImdbId(this.mediaId).pipe(finalize(() => this.omdbLoading = false)).subscribe((omdbRes) => {

      this.model = new AddMedia(omdbRes);
      if (isDefined(this.movieImage)) {
        this.model.imageUrl = this.movieImage;
      }
      this.fields = generateAddNewMoviesForm(this.allNames);
      if (isDefined(this.options.resetModel)) {
        this.options.resetModel();
      }
    });
  }
}
