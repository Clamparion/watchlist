import { MediaService } from './../services/media.service';
import { Watchlist } from './../../watchlist-overview/watchlist-overview-model';
import { Component, OnInit } from '@angular/core';
import { combineLatest, EMPTY } from 'rxjs';
import { map, catchError, takeUntil } from 'rxjs/operators';
import { Media, MediaType, MediaWatched } from 'src/app/watchlist/media-model';
import { FirestoreMediaService } from 'src/app/watchlist/services/firestore-media.service';
import { FirestoreWatchedMediaService } from 'src/app/watchlist/services/firestore-watched-media.service';
import { AuthService } from 'src/app/auth/services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { FirestoreUserService } from 'src/app/auth/services/firestore-user.sevice';
import { UserModel } from 'src/app/auth/models/user-model';
import { isDefined } from '@angular/compiler/src/util';
import { Router, ActivatedRoute } from '@angular/router';
import { DestroySubs } from 'src/app/destroy.sub';
import { FirestoreWatchlistService } from 'src/app/watchlist-overview/services/firestore-watchlist.service';

@Component({
  selector: 'app-movies',
  templateUrl: './watchlist.component.html',
  styleUrls: ['./watchlist.component.scss'],
})
export class WatchlistComponent extends DestroySubs implements OnInit {
  userId: string;
  items: Media[];
  filterdItems: Media[];
  filterBy: MediaType | 'all';
  filterText = '';
  watchlistId: string | undefined;
  watchlist: Watchlist;
  shareModalOpened = false;

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly mediaService: MediaService
  ) {
    super();
    this.setFilter('all');
  }

  ngOnInit(): void {
    this.route.queryParams
      .pipe(takeUntil(this.destroy$))
      .subscribe((params) => {
        this.setFilter(params.filter);
      });

    // route parameter
    this.route.paramMap.subscribe((params) => {
      if (params.get('id') === undefined) {
        this.router.navigate(['/']);
      } else {
        this.watchlistId = params.get('id');
        this.mediaService.listAll(this.watchlistId).subscribe(res => {
          this.items = res.media;
          this.watchlist = res.watchtlist;
          this.filterItems();
        });
      }
    });
  }

  filterItems() {
    if (isDefined(this.filterText) && this.filterText.length > 0) {
      const ft = this.filterText.toLowerCase();
      this.filterdItems = this.items.filter((item) => {
        return (
          item.title.toLowerCase().indexOf(ft) !== -1 ||
          item.genre.toLowerCase().indexOf(ft) !== -1 ||
          item.year.toString().indexOf(ft) !== -1 ||
          item.actors.toLowerCase().indexOf(ft) !== -1 ||
          item.watchedBy.join(',').toLowerCase().indexOf(ft) !== -1
        );
      });
    } else {
      this.filterdItems = this.items;
    }
  }

  navigateFilter(type: string | undefined) {
    this.setFilter(type);
    this.router.navigate(
      [this.route.snapshot.url.map((u) => u.path).join('/')],
      { queryParams: { filter: type } }
    );
  }

  setFilter(type: string) {
    switch (type) {
      case MediaType.Movie:
        this.filterBy = MediaType.Movie;
        break;
      case MediaType.Series:
        this.filterBy = MediaType.Series;
        break;
      default:
        this.filterBy = 'all';
    }
  }
}
