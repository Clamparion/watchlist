
import { Watchlist } from './../watchlist-overview/watchlist-overview-model';
import { isDefined } from '@angular/compiler/src/util';
import { OmdbResult } from 'src/app/imdb/imdb-model';
import { convertToFloat } from 'src/app/declaredFunctions';

export class WatchlistAndMediaWrapper {
  watchtlist: Watchlist;
  media: Media[];
}

export interface Media {
  mediaId: string;
  title: string;
  year: number;
  runtime: string;
  genre: string;
  director: string;
  actors: string;
  plot: string;
  country: string;
  awards: string;
  metascore: number;
  imdbRating: number;
  imdbId: string;
  type: MediaType;
  production: string;
  imageUrl: string;
  addedBy: string;
  watchedBy: string[];
  ownWatchId: string;
}

export interface MediaSearch {
  name: string;
}

export class MediaWatched {
  watchId: string;
  imdbId: string;
  userName: string;
  uid: string;
}

export enum MediaType {
  Movie = 'movie',
  Series = 'series',
}
export class AddMedia {
  title: string;
  year: number;
  runtime: string;
  genre: string;
  director: string;
  actors: string;
  plot: string;
  country: string;
  awards: string;
  metascore: number;
  imdbRating: number;
  type: MediaType;
  production: string;
  imageUrl: string;
  image: File[];
  imdbId: string;

  constructor(omdb: OmdbResult) {
    if (isDefined(omdb)) {
      this.title = omdb.Title;
      this.year = parseInt(omdb.Year, 10);
      this.runtime = omdb.Runtime;
      this.genre = omdb.Genre;
      this.director = omdb.Director;
      this.actors = omdb.Actors;
      this.plot = omdb.Plot;
      this.country = omdb.Country;
      this.awards = omdb.Awards;
      this.metascore = isNaN(convertToFloat(omdb.Metascore)) ? null : convertToFloat(omdb.Metascore);
      this.imdbRating = isNaN(convertToFloat(omdb.imdbRating)) ? null : convertToFloat(omdb.imdbRating);
      this.type = omdb.Type === 'movie' ? MediaType.Movie : MediaType.Series;
      this.imageUrl = omdb.Poster;
      this.image = null;
      this.imdbId = omdb.imdbID;
    } else {
      this.title = null;
      this.year = null;
      this.runtime = null;
      this.genre = null;
      this.director = null;
      this.actors = null;
      this.plot = null;
      this.country = null;
      this.awards = null;
      this.metascore = null;
      this.imdbRating = null;
      this.type = null;
      this.imageUrl = null;
      this.image = null;
      this.imdbId = null;
    }
  }

}
