import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAnalyticsModule } from '@angular/fire/analytics';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from '../environments/environment';

import { FormlyModule } from '@ngx-formly/core';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { validationMessages } from 'src/app/form-validation-messages';
import { LoginComponent } from './auth/components/login/login.component';
import { AuthService } from 'src/app/auth/services/auth.service';
import { NavComponent } from './nav/nav.component';
import { MediaCardComponent } from './watchlist/media-card/media-card.component';
import { ToastrModule } from 'ngx-toastr';
import {BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RegisterComponent } from './auth/components/register/register.component';
import { FormlyFieldFile } from 'src/app/formly/file-type.component';
import { FileValueAccessor } from 'src/app/formly/file-value-accessor';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { AddMediaFormComponent } from 'src/app/watchlist/add-media-wizard/add-media-form/add-media-form.component';
import { MediaTitleComponent } from './watchlist/add-media-wizard/media-title/media-title.component';
import { MediaWizardComponent } from './watchlist/add-media-wizard/media-wizard/media-wizard.component';
import { HttpClientModule } from '@angular/common/http';
import { AngularFireFunctionsModule } from '@angular/fire/functions';
import { NgxFontAwesomeModule } from 'ngx-font-awesome';
import { WatchlistComponent } from 'src/app/watchlist/watchlist/watchlist.component';
import { WatchlistOverviewComponent } from './watchlist-overview/watchlist-overview/watchlist-overview.component';
import { WatchlistCardComponent } from './watchlist-overview/watchlist-card/watchlist-card.component';
import { AddNewWatchlistComponent } from './watchlist-overview/add-new-watchlist/add-new-watchlist.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { WatchlistShareModalComponent } from './watchlist-invite/watchlist-share-modal/watchlist-share-modal.component';
import { WatchlistInviteComponent } from './watchlist-invite/watchlist-invite/watchlist-invite.component';
import { MediaDetailsPageComponent } from './watchlist/media-details-page/media-details-page.component';
import { MarkAsWatchedComponent } from './watchlist/mark-as-watched/mark-as-watched.component';
@NgModule({
  declarations: [
    AppComponent,
    WatchlistComponent,
    AddMediaFormComponent,
    MediaTitleComponent,
    LoginComponent,
    NavComponent,
    MediaCardComponent,
    RegisterComponent,
    FileValueAccessor,
    FormlyFieldFile,
    MediaTitleComponent,
    MediaWizardComponent,
    WatchlistOverviewComponent,
    WatchlistCardComponent,
    AddNewWatchlistComponent,
    WatchlistShareModalComponent,
    WatchlistInviteComponent,
    MediaDetailsPageComponent,
    MarkAsWatchedComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAnalyticsModule, // dynamically imports firebase/analytics
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
    AngularFireStorageModule, // imports firebase/storage only needed for storage features
    AngularFireFunctionsModule,
    ToastrModule.forRoot(),
    ButtonsModule.forRoot(),
    FormlyModule.forRoot(validationMessages),
    FormlyBootstrapModule,
    NgxFontAwesomeModule,
    ModalModule.forRoot(),
    FormlyModule.forRoot({
      types: [
        { name: 'file', component: FormlyFieldFile, wrappers: ['form-field'] },
      ],
    }),
  ],
  providers: [AuthService],
  bootstrap: [AppComponent],
})
export class AppModule {}
