import { isDefined } from '@angular/compiler/src/util';

export function convertToFloat(ipt: string | number): number {
  return isDefined(ipt) ? parseFloat(ipt.toString().replace(',', '.')) : null;
}


export const isEmptyArray = <T>(value: T[]): value is T[] => {
  return (value as T[]).length < 1;
};

export const isUndefinedValue = <T>(obj: T | null | undefined): obj is null | undefined => {
  return typeof obj === 'undefined' || obj === null;
};
