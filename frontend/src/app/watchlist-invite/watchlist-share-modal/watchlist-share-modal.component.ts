import { isDefined } from '@angular/compiler/src/util';
import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { Watchlist } from 'src/app/watchlist-overview/watchlist-overview-model';
import { WatchlistInviteService } from '../watchlist-invite.service';

@Component({
  selector: 'app-watchlist-share-modal',
  templateUrl: './watchlist-share-modal.component.html',
  styleUrls: ['./watchlist-share-modal.component.scss'],
})
export class WatchlistShareModalComponent implements OnInit {
  @ViewChild('shareModal') modalTemplate: TemplateRef<any>;
  modalRef: BsModalRef;
  isModalOpened: boolean;

  @Input() watchlist: Watchlist;

  get isOpened(): boolean {
    return this.isModalOpened;
  }
  @Input('isOpened')
  set isOpened(value: boolean) {
    this.isModalOpened = value;
    if (this.isModalOpened && isDefined(this.modalTemplate)) {
      this.createShareLink();
      this.modalRef = this.modalService.show(this.modalTemplate);
    } else if (isDefined(this.modalRef)) {
      this.modalRef.hide();
    }
  }

  @Output() isOpenedChange = new EventEmitter<boolean>();

  shareLink: string;
  constructor(
    private readonly watchlistInviteService: WatchlistInviteService,
    private readonly modalService: BsModalService,
    private readonly toastr: ToastrService
  ) {}

  copyInputMessage(inputElement) {
    inputElement.select();
    document.execCommand('copy');
    inputElement.setSelectionRange(0, 0);
    this.toastr.success(
      'Der Link wurde erfolgreich in die Zwischenabglage kopiert.',
      'Link kopiert!'
    );
  }

  private createShareLink() {
    if (isDefined(this.watchlist)) {
      this.watchlistInviteService.createShareLink(this.watchlist).then(res => {
        this.shareLink = res;
      }).catch(err => console.log(err));
    }
  }

  public hide() {
    this.modalRef.hide();
    this.isModalOpened = false;
    this.isOpenedChange.next(false);
  }

  ngOnInit(): void {}
}
