import { FirestoreWatchlistService } from 'src/app/watchlist-overview/services/firestore-watchlist.service';
import { isDefined } from '@angular/compiler/src/util';
import { AuthService } from './../auth/services/auth.service';
import { Watchlist } from './../watchlist-overview/watchlist-overview-model';
import { Injectable } from '@angular/core';

const LOKAL_KEY = 'invitedWatchlist';

@Injectable({
  providedIn: 'root'
})
export class WatchlistInviteService {


  constructor(private auth: AuthService, private readonly watchlistService: FirestoreWatchlistService) { }

  isAlreadyInvited(watchlistId: string) {
    return this.getWachtlistIdFromLocalstorage() === watchlistId;
  }


  async processInvitation(): Promise<boolean> {
    const user = await this.auth.getUserAsPromise();
    const watchlistId = this.getWachtlistIdFromLocalstorage();
    if (isDefined(user) && isDefined(user.uid) && isDefined(watchlistId)) {
      await this.watchlistService.assignWatchListToUser(watchlistId, user.uid);
      await this.watchlistService.assignUserToWatchList(watchlistId, user.uid);
      localStorage.removeItem(LOKAL_KEY);
      return true;
    }
    return false;
  }

  acceptInvitation(watchlistId: string): void {
    localStorage.setItem(LOKAL_KEY, watchlistId);
  }

  private getWachtlistIdFromLocalstorage(): string {
    return localStorage.getItem(LOKAL_KEY);
  }

  async isLoggedIn(): Promise<boolean> {
    const user = await this.auth.getUserAsPromise();
    return isDefined(user) && isDefined(user.uid);
  }


  async createShareLink(watchlist: Watchlist): Promise<string> {
    const user = await this.auth.getUserAsPromise();
    return encodeURI(`${window.location.origin}/wl/#/invite/${watchlist.watchlistId}?title=${watchlist.name}&from=${user.displayName}`);
  }
}
