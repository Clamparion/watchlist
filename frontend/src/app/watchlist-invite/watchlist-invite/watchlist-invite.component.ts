import { WatchlistInviteService } from './../watchlist-invite.service';

import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-watchlist-invite',
  templateUrl: './watchlist-invite.component.html',
  styleUrls: ['./watchlist-invite.component.scss'],
})
export class WatchlistInviteComponent implements OnInit {
  title: string;
  from: string;
  watchListId: string;

  showMustLogInMessage = false;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly wachtlistInvite: WatchlistInviteService
  ) {
    this.route.queryParams.subscribe((queryParams) => {
      this.title = queryParams.title;
      this.from = queryParams.from;
    });
    this.route.paramMap.subscribe((params) => {
      this.watchListId = params.get('watchlistId');
      if (this.wachtlistInvite.isAlreadyInvited(this.watchListId)) {
        this.invitationCompleted();
      }
    });
  }

  acceptInvitation() {
    this.wachtlistInvite.acceptInvitation(this.watchListId);
    this.invitationCompleted();
  }

  invitationCompleted() {
    this.wachtlistInvite.isLoggedIn().then((loggedIn) => {
      if (loggedIn) {
        this.router.navigate(['']);
      } else {
        this.showMustLogInMessage = true;
      }
    });
  }

  ngOnInit(): void {}
}
