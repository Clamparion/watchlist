import { Subject } from 'rxjs';
import { OnDestroy } from '@angular/core';

export abstract class DestroySubs implements OnDestroy  {
  destroy$: Subject<boolean> = new Subject<boolean>();

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }
}
