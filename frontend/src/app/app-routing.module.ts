import { MediaDetailsPageComponent } from './watchlist/media-details-page/media-details-page.component';
import { WatchlistInviteComponent } from './watchlist-invite/watchlist-invite/watchlist-invite.component';
import { AddNewWatchlistComponent } from './watchlist-overview/add-new-watchlist/add-new-watchlist.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from 'src/app/auth/components/login/login.component';
import { AuthGuard } from 'src/app/auth/services/auth.guard';
import { RegisterComponent } from 'src/app/auth/components/register/register.component';
import { MediaWizardComponent } from 'src/app/watchlist/add-media-wizard/media-wizard/media-wizard.component';
import { WatchlistComponent } from 'src/app/watchlist/watchlist/watchlist.component';
import { WatchlistOverviewComponent } from './watchlist-overview/watchlist-overview/watchlist-overview.component';

const routes: Routes = [
  {
    path: 'invite/:watchlistId',
    pathMatch: 'full',
    component: WatchlistInviteComponent
  },
  {
    path: '',
    pathMatch: 'full',
    component: WatchlistOverviewComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'add',
    component: AddNewWatchlistComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'watchlist/:id',
    component: WatchlistComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'watchlist/:id/details/:mediaId',
    component: MediaDetailsPageComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'watchlist/:id/add',
    component: MediaWizardComponent,
    canActivate: [AuthGuard]
  },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
