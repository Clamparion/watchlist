import { ConfigOption, FormlyFieldConfig } from '@ngx-formly/core';
import { isDefined } from '@angular/compiler/src/util';

export const positiveIntPattern = /^[1-9]\d*$/;
export const positiveFloatPattern = /^(?=.+)(?:[1-9]\d*|0)?(?:(\.|,)\d+)?$/;
// tslint:disable-next-line: max-line-length
export const emailPattern = /^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
export function requiredMessage(err, field: FormlyFieldConfig) {
  if (
    isDefined(field) &&
    isDefined(field.templateOptions) &&
    isDefined(field.templateOptions.label)
  ) {
    return `Das Feld "${field.templateOptions.label}" ist erforderlich`;
  }
  return 'Dieses Feld ist erfordelich';
}

export function patternMessage(err, field: FormlyFieldConfig) {
  if (
    isDefined(field) &&
    isDefined(field.templateOptions) &&
    isDefined(field.templateOptions.pattern)
  ) {
    switch (field.templateOptions.pattern) {
      case positiveIntPattern:
        return 'Bitte nur positive Ganzzahlen eingeben';
      case positiveFloatPattern:
        return 'Bitte nur positive Kommazahlen eingeben';
      case emailPattern:
        return 'Bitte geben sie eine valide Email-Adresse ein';
      default:
        break;
    }
  }
  return 'Die Eingabe entspricht nicht den Vorgaben';
}

export const validationMessages: ConfigOption = {
  validationMessages: [
    { name: 'required', message: requiredMessage },
    { name: 'pattern', message: patternMessage },
  ],
};
