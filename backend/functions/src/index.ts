import * as functions from 'firebase-functions';
import { search } from './imdb';
import * as admin from 'firebase-admin';

admin.initializeApp();

//auto block users on signup
exports.blockSignup = functions.auth.user().onCreate((event) => {
  return admin.firestore().collection('userPermissions').doc(event.uid).set({
    disabled: true,
    name: event.displayName,
    email: event.email,
  }).then(() => console.log('Auto blocked user', event.toJSON()))
  .catch((error) => console.log('Error auto blocking:', error));
});

export const imdb = functions.https.onCall((data, context) => {
  if (!isAuthenticated(context)) {
    throw new functions.https.HttpsError('unauthenticated', 'not logged in');
  }

  const searchString = data.s;
  if (typeof searchString !== 'string' || searchString.length < 1) {
    throw new functions.https.HttpsError(
      'invalid-argument',
      'no search string provided'
    );
  }

  return search(searchString)
    .then((res) => res)
    .catch((err) => {
      throw new functions.https.HttpsError('internal', err);
    });
});

function isAuthenticated(context: functions.https.CallableContext) {
  return context.auth?.uid !== null;
}
