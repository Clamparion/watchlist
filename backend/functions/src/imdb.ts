const httpRequest = require('request');
export class ImdbApiSearchResults {
  q: string | undefined; // search string
  d: ImdbApiSearchResult[] | undefined;
}

export class ImdbApiSearchResult {
  l: string | undefined; // title
  id: string | undefined; // imdb id
  s: string | undefined; // actors
  y: string | undefined; // year
  i: ImdbImage | undefined; // imgae,
}

export class ImdbImage {
  height: number | undefined;
  imageUrl: string | undefined;
  width: number | undefined;
}

export class ImdbApiSearchResultReadable {
  title: string | undefined; // title
  id: string | undefined; // imdb id
  actors: string | undefined; // actors
  year: string | undefined; // year
  image: string | undefined; // imgae,
  constructor(result: ImdbApiSearchResult) {
    this.title = result.l;
    this.id = result.id;
    this.actors = result.s;
    this.year = result.y;
    if (
      result.i !== undefined &&
      result.i.imageUrl !== undefined &&
      typeof result.i.imageUrl === 'string'
    ) {
      this.image =  result.i.imageUrl;
      this.image = this.image.replace('@._V1_', '@._V1_UY268_CR3,0,182,268');
    }
  }
}


export function search(text: string): Promise<ImdbApiSearchResultReadable[]> {
  return new Promise<ImdbApiSearchResultReadable[]>((resolve, reject) => {
    const searchtext = deUmlaut(text.toLowerCase().trim());
    const firstChar = searchtext.charAt(0);
    const url = `https://v2.sg.media-imdb.com/suggestion/${encodeURIComponent(
      firstChar
    )}/${encodeURIComponent(searchtext)}.json`;

    httpRequest(
      url,
      { json: true },
      (err: any, res: any, body: ImdbApiSearchResults) => {
        if (err) {
          reject(err);
        }
        if (
          body !== undefined &&
          body.d !== undefined &&
          Array.isArray(body.d)
        ) {
          const results = body.d.map(
            (searchRes) => new ImdbApiSearchResultReadable(searchRes)
          );
          resolve(results);
        } else {
          reject('nothing found');
        }
      }
    );
  });
}

export function deUmlaut(val: string): string {
  let value = val;
  value = value.toLowerCase();
  value = value.replace(/ä/g, 'ae');
  value = value.replace(/ö/g, 'oe');
  value = value.replace(/ü/g, 'ue');
  value = value.replace(/ß/g, 'ss');
  value = value.replace(/ /g, '-');
  value = value.replace(/\./g, '');
  value = value.replace(/,/g, '');
  value = value.replace(/\(/g, '');
  value = value.replace(/\)/g, '');
  return value;
}
